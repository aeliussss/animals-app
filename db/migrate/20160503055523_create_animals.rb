class CreateAnimals < ActiveRecord::Migration
  def change
    create_table :animals do |t|
      t.string :name
      t.string :the_type
      t.string :mood

      t.timestamps null: false
    end
  end
end
