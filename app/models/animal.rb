class Animal < ActiveRecord::Base
    validates :name,  presence: true, length: { maximum: 50 }
    validates :the_type,  presence: true, length: { maximum: 50 }
    validates :mood, presence: true, length: {maximum: 50}
    validate :validatemood 
    
    
    def validatemood
        if mood != "shy" || mood != "vicious" || mood != "tame"
            errors.add(:mood, "- An animal can only be shy, tame or vicious.");
        end
    end
end
