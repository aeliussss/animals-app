json.array!(@animals) do |animal|
  json.extract! animal, :id, :name, :the_type, :mood
  json.url animal_url(animal, format: :json)
end
